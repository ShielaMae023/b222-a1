"""s01_activity URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('django_practice/', include('django_practice.urls')),
    path('admin/', admin.site.urls),
        path('', views.index, name='index'),
    path('<int:grocerylist_id>/', views.grocerylist, name='viewgrocerylist'),
    # /todolist/register
    path('register', views.register, name="register"),
    # /todolist/change_password
    path('change_password', views.change_password, name='change_password'),
    # /todolist/login
    path('login', views.login_view, name='login'),
    # /todolist/logout
    path('logout', views.logout_view, name="logout")
]
