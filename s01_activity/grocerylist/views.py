from django.shortcuts import render, redirect
from .models import grocerylist
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
# Create your views here.

def index(request):
	grocery_list = grocerylist.objects.all()
	# output = ', '.join([todoitem.task_name for todoitem in todoitem_list])
	# template = loader.get_template("todolist/index.html")
	context = {'grocerylist_item': grocerylist_list}
	return render(request, "grocerylist/index.html", context)

def grocerylist(request, grocerylist_id):
	response = "You are viewing the details of %s"
	return HttpResponse(response % grocerylist_id)

def register(request):
	users = User.objects.all()
	is_user_registered = False
	context = {
		"is_user_registered": is_user_registered
	}

	for indiv_user in users:
		if indiv_user.username == "johndoe":
			is_user_registered = True
			break


	if is_user_registered == False:
		user = User()
		user.username = "johndoe"
		user.first_name = "John"
		user.last_name = "Doe"
		user.email = "john@mail.com"
		user.set_password("john1234")
		user.is_staff = False
		user.is_active = True
		user.save()
		context ={
			"first_name": user.first_name,
			"last_name": user.last_name
		}

	return render(request, "grocerylist/register.html", context)

def change_password(request):

	is_user_authenticated = False

	user = authenticate(username="johndoe", password="john1234")
	print(user)
	if user is not None:
		authenticated_user = User.objects.get(username='johndoe')
		authenticated_user.set_password("johndoe1")
		authenticated_user.save()
		is_user_authenticated = True
		context = {
			"is_user_authenticated": is_user_authenticated
		}

		return render(request, "grocerylist/change_password.html", context)

def login_view(request):
	username = "johndoe"
	password = "johndoe1"
	user = authenticate(username=username, password=password)
	context = {
		"is_user_authenticated": False
	}
	print(user)
	if user is not None:
		login(request, user)
		return redirect("index")
	else:
		return render(request, "grocerylist/login.html", context)

def logout_view(request):
	logout(request)
	return redirect("index")

